# Cloud images

### Image building

note: docker images moved to separate projects (linuxsupport/{cc7-base,slc6-base,slc5-base})

#### How to build:

See ```buildimage.sh``` script.

### Images:

All images are made available in 'IT Linux Support - Images' Openstack.cern.ch Project. (See ```upload2openstack.sh``` script.)

#### Publishing Images

There are Gitlab CI jobs that make new images public and mark the old ones as 'community'.
If for some reason you would like to exclude a public image from this process, you can edit
it's metadata to change the value of the property `gitops`.

[Tests](https://gitlab.cern.ch/linuxsupport/testing/) are in place to check everything is working as expected. If you want to skip them, run a pipeline with variable `CI_SKIP_TESTS="true"`. This can be done through CI/CD -> Pipelines -> Run pipeline -> Select branch, add the variable, press run. This will override globally defined variables in the CI.

A scheduled pipeline rebuilds the image once per month.

##### CERN CentOS 7:

```20180612: http://koji.cern.ch/koji/taskinfo?taskID=1152019```

```20180516: http://koji.cern.ch/koji/taskinfo?taskID=1127400``` (CC7.5)

```20180316: http://koji.cern.ch/koji/taskinfo?taskID=1075007```

```20180112: http://koji.cern.ch/koji/taskinfo?taskID=1020936```

```20171114: http://koji.cern.ch/koji/taskinfo?taskID=973565```

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=927884```


##### Scientific Linux CERN 6:

```20188622: http://koji.cern.ch/koji/taskinfo?taskID=1162224``` (SLC6.10)

```20180515: http://koji.cern.ch/koji/taskinfo?taskID=1125954```

```20180316: http://koji.cern.ch/koji/taskinfo?taskID=1075006```

```20180112: http://koji.cern.ch/koji/taskinfo?taskID=1020933```

```20171114: http://koji.cern.ch/koji/taskinfo?taskID=973544```

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=927881```


##### Scientific Linux CERN 5:

```20180316: http://koji.cern.ch/koji/taskinfo?taskID=1075002```

```20180112: http://koji.cern.ch/koji/taskinfo?taskID=1020930```

```20171114: http://koji.cern.ch/koji/taskinfo?taskID=973541```

```20170920: http://koji.cern.ch/koji/taskinfo?taskID=927878```

##### Red Hat Enterprise Linux images

 These are upstream RHEL images converted from qcow2 to raw format (convertimage.sh),
downloaded from 

```https://access.redhat.com/downloads/```

uploaded with ```upload2openstack.sh``` then shared using:

```openstack image add project image-id project-id```

for following project ids:

```bbe4955e-c341-42b2-9369-88ea404083e5  (IT Database Cloud 01)```

```cff803b0-0eb4-460f-821d-aab97e531e38  (IT Database Cloud 02 Test)```

```a1a59b6f-9488-4ce3-8660-1e92dea71950  (IT Dayabase Ironic 01)```

```5973a01d-94ed-4780-bdfc-919a469fb882  (IT Dayabase Ironic 01 Test)```



(then contact IT/DB and tell them to accept images with:
```
openstack image set --accept image-id
```
)....


##### CERN CentOS 7.3 image for CMS

Share with 'CMS Data Acquisition':

CC7 - x86_64 [2017-01-24] 77b4efd5-dfb4-4330-81a1-d930184b2533
CC7 TEST - x86_64 [2017-01-13] 43c71fe1-71b8-4225-8a37-58c9279914f5

```openstack image add project XXX 63176c63-4cb9-4bcc-bdaf-158437b881c5```
ask to accept with:
```openstack image set --accept XXX```

### NOTES:

slc6 -> ksversion RHEL7 <- otherwise kickstart %packages --nocore not acccepted.

slc5 -> ksversion RHEL5 <- otherwise kickstart sections not ending in %end fail.

