#version=RHEL7
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration for SSH
firewall --enabled --service=ssh
# No firstboot
firstboot --disable
# Network information
network  --bootproto=dhcp
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump" --enabled="sshd"
# License agreement
eula --agreed
# System timezone
timezone --utc Europe/Zurich
# this should not be neeed
ignoredisk --only-use=vda
# Clear the Master Boot Record
zerombr

# Partitioning and bootloader configuration
# Note: biosboot and efi partitions are pre-created in %pre.

bootloader --location=mbr --timeout=1 --append="console=ttyS0,115200 console=tty0" --boot-drive=vda
# This is replaced by the pre-creation of partitions by sgdisk
#part biosboot --onpart=vda14 --size=4
part /boot/efi --onpart=vda15 --fstype=vfat
part / --fstype="xfs" --size=1 --grow --asprimary

%pre --log=/var/log/anaconda/pre-install.log --erroronfail
#!/bin/bash

# Pre-create the biosboot and EFI partitions
#  - Ensure that efi and biosboot are created at the start of the disk to
#    allow resizing of the OS disk.
#  - Label biosboot and efi as vda14/vda15 for better compat - some tools
#    may assume that vda1/vda2 are '/boot' and '/' respectively.
sgdisk --clear /dev/vda
sgdisk --new=14:2048:10239 /dev/vda
# Size must be 50MB at least
sgdisk --new=15:10240:128M /dev/vda
# Add hex codes to label partitions appropriately as KS part command does
# EF02 hex code: BIOS boot partition
sgdisk --typecode=14:EF02 /dev/vda
# EF00 hex code: EFI System
sgdisk --typecode=15:EF00 /dev/vda

%end

reboot

# External repos are added by Koji tags
%packages
@^minimal-environment
-biosdevname
-iprutils
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6050-firmware
-iwl7260-firmware
CERN-CA-certs
epel-release
krb5-workstation
nfs-utils
grub2
grub2-efi-x64-modules
efibootmgr
parted
gdisk
redhat-lsb-core
rsync
tar
vim-enhanced
yum-utils
%end

%post --log=/root/anaconda-post.log

yum -y install cloud-init cloud-utils-growpart cern-get-keytab

# lock root account
passwd -d root
passwd -l root

# Generate new machine-id on first boot
truncate --size 0 /etc/machine-id

rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: 1|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8

# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,pre,post}.log
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/dnf/*

# Ironic fix for RAID1 at boot time (Disabled for now)
/usr/bin/dracut -v --add 'mdraid' --add-drivers 'raid1 raid10 raid5 raid0 linear' --regenerate-all --force

# Ironic fix rd.auto
sed -i -e 's/crashkernel=auto/rd.auto net.ifnames=0 crashkernel=auto/g' /etc/default/grub

# We are installing bootloader with the bootloader command, but this only adds bios compatible (as we are building in a UEFI VM), so we force both: https://unix.stackexchange.com/questions/273329/can-i-install-grub2-on-a-flash-drive-to-boot-both-bios-and-uefi
# Enable BIOS bootloader
grub2-mkconfig --output /etc/grub2-efi.cfg
grub2-install --target=i386-pc --directory=/usr/lib/grub/i386-pc/ /dev/vda
grub2-mkconfig --output=/boot/grub2/grub.cfg



# Fix grub.cfg to remove EFI entries and adapt them to BIOS

# blkid --match-tag UUID --output value /dev/vda15 on C8!!!!!
EFI_ID=`blkid -s UUID -o value /dev/vda15`
BOOT_ID=`blkid -s UUID -o value /dev/vda1`
sed -i 's/gpt15/gpt1/' /boot/grub2/grub.cfg
sed -i "s/${EFI_ID}/${BOOT_ID}/" /boot/grub2/grub.cfg
sed -i 's|${config_directory}/grubenv|(hd0,gpt15)/efi/centos/grubenv|' /boot/grub2/grub.cfg
sed -i '/^### BEGIN \/etc\/grub.d\/30_uefi/,/^### END \/etc\/grub.d\/30_uefi/{/^### BEGIN \/etc\/grub.d\/30_uefi/!{/^### END \/etc\/grub.d\/30_uefi/!d}}' /boot/grub2/grub.cfg

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end
