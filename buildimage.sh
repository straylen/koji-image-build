#!/bin/bash

#temp script: TBD better.

function usage {
 echo "`basename $0` major {release}"
 echo "         major = 5,6,7,8"
 echo "         release = scratch/release"
}

[ -z $1 ] && usage && exit 1
[ -z $2 ] && usage && exit 1
SCRATCH="--scratch"
[ $2 == "release" ] && SCRATCH=""
VER=$1

URL="http://linuxsoft.cern.ch/cern"
KSURL="git+ssh://git@gitlab.cern.ch:7999/${CI_PROJECT_PATH}#${CI_COMMIT_SHORT_SHA}"
FORMAT="raw"
DATE=`date "+%Y%m%d"`
EXTRA_ARGS=""

case $VER in
    5)
        NAME="slc5-cloud"
        TAG="slc5-image-5x"
        URL="${URL}/slc5X/\$arch"
        KSFILE="${NAME}.ks"
        DISTRO="RHEL-5.11"
        KSVER="RHEL5"
        ARCH="i386 x86_64"
        DISK_SIZE="3"
    ;;
    6)
        NAME="slc6-cloud"
        TAG="slc6-image-6x"
        URL="${URL}/slc69/\$arch"
        KSFILE="${NAME}.ks"
        DISTRO="RHEL-6.9"
        KSVER="RHEL7"
        ARCH="i386 x86_64"
        DISK_SIZE="3"
    ;;
    7)
        NAME="cc7-cloud"
        TAG="cc7-image-7x"
        ARCH="x86_64"
        # temp: 7.2 instead of 7: libguestfs problems with xfs 7.2->7.3 changes
        URL="${URL}/centos/7.8/os/\$arch"
        KSFILE="${NAME}.ks"
        DISTRO="RHEL-7.5"
        KSVER="RHEL7"
        ARCH="x86_64"
        DISK_SIZE="4"
        EXTRA_ARGS="--factory-parameter=generate_icicle False"
    ;;
    8)
        NAME="c8-cloud"
        TAG="c8-image-8x"
        ARCH="x86_64"
        URL="${URL}/centos/8/BaseOS/\$arch/kickstart/"
        KSFILE="${NAME}.ks"
        # An update to imagefactory is required before we can change the below ref
        DISTRO="RHEL-7.5"
        KSVER="RHEL7"
        ARCH="x86_64"
        DISK_SIZE="4"
        # We pass this to avoid guestfish operations on the guest after installation
        # EL8 comes with new features in XFS, but our builders are EL7. Without the
        # below, we see failed builds
        EXTRA_ARGS="--factory-parameter=generate_icicle False"
    ;;
esac

echo "executing: koji image-build --wait ${NAME} ${DATE} ${TAG} ${URL} ${ARCH} --ksurl=${KSURL} --kickstart=${KSFILE} --distro=${DISTRO} --format=${FORMAT} --disk-size=${DISK_SIZE} --ksversion=${KSVER} ${EXTRA_ARGS} ${SCRATCH}"
sleep 3
koji image-build --wait ${NAME} ${DATE} ${TAG} ${URL} ${ARCH} --ksurl=${KSURL} --kickstart=${KSFILE} --distro=${DISTRO} --format=${FORMAT} --disk-size=${DISK_SIZE} --ksversion=${KSVER} ${EXTRA_ARGS} ${SCRATCH}
