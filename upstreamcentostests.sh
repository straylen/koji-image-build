#!/bin/bash
# The following script runs Upstream CentOS functional tests with some tweaks

yum install git cern-linuxsupport-access -y

cern-linuxsupport-access enable
git clone https://gitlab.cern.ch/linuxsupport/centos_functional_tests.git
cd centos_functional_tests;

# Disable certain tests from upstream
# We will add CERN specific tests on https://gitlab.cern.ch/linuxsupport/testing/cern_centos_functional_tests if needed
cat > ./skipped-tests.list  <<DELIM
# This file contains list of tests we need/want to skip
# Reason is when there is upstream bug that we're aware of 
# So this file should contain:
#  - centos version (using $centos_ver)
#  - test to skip (tests/p_${name}/test.sh)
#  - reason why it's actually skipped (url to upstream BZ, or bug report)
# Separated by |
8|tests/p_gzip/30-gzexe-test|https://apps.centos.org/kanboard/project/23/task/833
8|tests/p_diffutils/10-cmp-tests|https://bugzilla.redhat.com/show_bug.cgi?id=1732960
7|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
8|tests/0_common/00_centos_repos.sh|We want to keep CERN repos enabled
7|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
8|tests/0_common/20_upgrade_all.sh|Avoid too much noise on CI logs
7|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
8|tests/0_common/30_dns_works.sh|We spawn VM without waiting for DNS register
7|tests/p_arpwatch/*|It is omitted in cc7 anyway
8|tests/p_arpwatch/*|arpwatch always ends up in coredump in c8
7|tests/p_cron/*|Avoid spamming VM owner with cron tests
8|tests/p_cron/*|Avoid spamming VM owner with cron tests
7|tests/p_ntp/*|We have our own NTP servers, checking in a separate test
8|tests/p_ntp/*|We have our own NTP servers, checking in a separate test
7|tests/p_ipa-server/*|No Freeipa default tests, also, we use cern-get-keytab
8|tests/p_ipa-server/*|No Freeipa default tests, also, we use cern-get-keytab
7|tests/p_postfix/*|No postfix tests are required, avoid spamming node owner
8|tests/p_postfix/*|No postfix tests are required, avoid spamming node owner
7|tests/p_sendmail/*|No mail tests are required, avoid spamming node owner
8|tests/p_sendmail/*|No mail tests are required, avoid spamming node owner
7|tests/p_yum-plugin-fastestmirror/*|CERN CentOS does not have mirror list enabled
8|tests/p_yum-plugin-fastestmirror/*|CERN CentOS does not have mirror list enabled
7|tests/r_check_mod_packages/*|Does not apply for CCentOS
8|tests/r_check_mod_packages/*|Does not apply for CCentOS
7|tests/z_rpminfo/*|Does not apply in our case
8|tests/z_rpminfo/*|Does not apply in our case
8|tests/p_httpd/*|httpd results in 403 for all requests, disabling for now
7|tests/p_squid/*|squid seems to fail from time to time, disabling for now
8|tests/p_squid/*|squid seems to always fail, disabling for now
DELIM

# Tweaks for test suite to work on CERN CentOS
# Need to tweak Pam so passwd tests do not fail due to test user being taken under Kerberos auth
sed -i '/password\s*sufficient\s*pam_krb5.so\s*use_authtok/d' /etc/pam.d/system-auth &> /dev/null

./runtests.sh