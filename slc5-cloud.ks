#version=RHEL5
# System authorization information
authconfig --enableshadow --enablemd5
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration AFS and ARCD
firewall --enabled --port=22:tcp --port=7001:udp --port=4241:tcp
# No firstboot
firstboot --disable
# Network information (onboot=yes needed!)
network  --bootproto=dhcp --onboot=yes
# Keyboard layouts
keyboard us
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
# services --disabled="kdump,rhsmcertd,wpa_supplicant,yum-updatesd,firstboot,bluetooth,conmani,smartd" --enabled="network,sshd,rsyslog"
# RH key not used on SLC
#key --skip
# System timezone
timezone --utc Europe/Zurich
# this should not be neeed
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --location=mbr --append="console=ttyS0,115200 console=tty0"
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --initlabel 
# Disk partitioning information       #2048 too small cause of %post yum update
part / --fstype="ext3" --ondisk=vda --size=3000 # needs to be < 3G for oz img size.
# Reboot after installation
reboot

#
# NOTE: we hacked kojid to NOT replace repos for 5 !
#       (or install fails as yum does not understand 
#       mergerepos produced metadata ..)
#

# installation path, additional repositories
repo --name="SLC5 - os" --baseurl http://linuxsoft.cern.ch/cern/slc5X/$basearch/
# repo order is *important* for koji image build target !
#repo --name="EPEL"             --baseurl http://linuxsoft.cern.ch/epel/5/$basearch/
#repo --name="SLC5 - updates"   --baseurl http://linuxsoft.cern.ch/cern/slc5X/$basearch/yum/updates/
#repo --name="SLC5 - extras"    --baseurl http://linuxsoft.cern.ch/cern/slc5X/$basearch/yum/extras/
# temp.
#repo --name="SLC5 - updates testing"   --baseurl http://linuxsoft.cern.ch/cern/slc5X/$basearch/yum/testing/




%packages
@cern-addons
@quattor-client
@openafs-client
cern-config-users
redhat-lsb
vim-enhanced
-lcm-firstboot
-conman
-bluez-gnome
-bluez-utils
-bluez-libs
-cdrecord
-Deployment_Guide-en-US
-eject
-irda-utils
-pcsc-lite-libs
-pcsc-lite
-coolkey
-ccid
-ifd-egate
-rhel-instnum
-rhpl
-firstboot
-system-config-network-tui
-firstboot-tui
-wireless-tools
-xorg-x11-filesystem


%post

# redirect the output to the log file
exec >/root/anaconda-post.log 2>&1

# lock root account
passwd -d root
passwd -l root

#slc5-testing -> temp
yum --enablerepo=slc5-testing -y install cern-private-cloud-addons
yum -y install cloud-init
yum -y update
# no yum here
rpm -e kernel-2.6.18-398.el5 kernel-module-openafs-2.6.18-398.el5
rpm -e kernel-PAE-2.6.18-398.el5 kernel-module-openafs-2.6.18-398.el5PAE
#
yum clean all

if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: 1|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg
fi

cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1 
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0


#tuned-adm profile virtual-guest

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-sl
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-5

# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-ks.cfg
rm -f /root/install.log{,.syslog}

# resize root partition (fs is resized by cloud)
# on first boot then reboot for fs resize

cat >> /etc/rc.sysinit << EOF
if [ ! -f /etc/.resize_root_part ]; then
 echo "resizing / partition /dev/vda1"
 # do it only once.
 touch /etc/.resize_root_part
 # new label | new pri part 1 | active | write
 echo -e "o\nn\np\n1\n\n\na\n1\nw\n" | fdisk /dev/vda
 # and we need grub back 
 grub-install /dev/vda
 sync
 echo "rebooting now for / fs resize"
 reboot
fi
EOF

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
