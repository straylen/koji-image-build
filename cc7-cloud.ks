#version=RHEL7
#perform install
install
# System authorization information
auth --enableshadow --passalgo=sha512
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration AFS and ARCD
firewall --enabled --service=ssh --port=7001:udp --port=4241:tcp
# No firstboot
firstboot --disable
# Network information
network  --bootproto=dhcp
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump,rhsmcertd,wpa_supplicant" --enabled="network,sshd,rsyslog"
# License agreement
eula --agreed
# System timezone
timezone --utc Europe/Zurich
# this should not be neeed
ignoredisk --only-use=vda
# Clear the Master Boot Record
zerombr

# Partitioning and bootloader configuration
# Note: biosboot and efi partitions are pre-created in %pre.

bootloader --location=mbr --timeout=1 --append="console=ttyS0,115200 console=tty0" --boot-drive=vda
# This is replaced by the pre-creation of partitions by sgdisk
#part biosboot --onpart=vda14 --size=4
part /boot/efi --onpart=vda15 --fstype=vfat
part / --fstype="xfs" --size=1 --grow --asprimary

%pre --log=/var/log/anaconda/pre-install.log --erroronfail
#!/bin/bash

# Pre-create the biosboot and EFI partitions
#  - Ensure that efi and biosboot are created at the start of the disk to
#    allow resizing of the OS disk.
#  - Label biosboot and efi as vda14/vda15 for better compat - some tools
#    may assume that vda1/vda2 are '/boot' and '/' respectively.
sgdisk --clear /dev/vda
sgdisk --new=14:2048:10239 /dev/vda
# Size must be 50MB at least
sgdisk --new=15:10240:128M /dev/vda
# Add hex codes to label partitions appropriately as KS part command does
# EF02 hex code: BIOS boot partition
sgdisk --typecode=14:EF02 /dev/vda
# EF00 hex code: EFI System
sgdisk --typecode=15:EF00 /dev/vda

%end


# overlay for XFS/docker: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/7.2_Release_Notes/technology-preview-file_systems.html
# Reboot after installation
reboot

repo --name="CentOS-7 - Base" --baseurl http://linuxsoft.cern.ch/cern/centos/7.8/os/$basearch/
# The following repo lines are commented as our koji tag builds already define these as external repos
#repo order is *important* for koji image build target !
#repo --name="CentOS-7 - CERN" --baseurl http://linuxsoft.cern.ch/cern/centos/7/cern/$basearch/
#repo --name="CentOS-7 - Updates" --baseurl http://linuxsoft.cern.ch/cern/centos/7/updates/$basearch/
#repo --name="CentOS-7 - Extras" --baseurl http://linuxsoft.cern.ch/cern/centos/7/extras/$basearch/

%packages
@cern-base
@cern-openafs-client
chrony
dracut-config-generic
dracut-norescue
firewalld
grub2
grub2-efi-x64-modules
efibootmgr
parted
gdisk
iptables-services
kernel
krb5-workstation
redhat-lsb-core
nfs-utils
ntp
rsync
tar
vim-enhanced
yum-utils
yum-plugin-ovl
mdadm
-aic94xx-firmware
-alsa-firmware
-alsa-lib
-alsa-tools-firmware
-biosdevname
-lcm-firstboot
-iprutils
-ivtv-firmware
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6000g2b-firmware
-iwl6050-firmware
-iwl7260-firmware
-iwl7265-firmware
-libertas-sd8686-firmware
-libertas-sd8787-firmware
-libertas-usb8388-firmware
-linux-firmware
-mcelog
-plymouth
-xorg-x11-font-utils
cloud-init
cloud-utils-growpart
cern-config-users
cern-private-cloud-addons
NetworkManager-DUID-LLT
mesa-libGL
locmap
nscd
sendmail-cf
%end

# not taken into account by image factory ...
%addon cern_customizations  --run-locmap --afs-client --auto-update
%end

%post --log=/root/anaconda-post.log

# lock root account
passwd -d root
passwd -l root

# Generate new machine-id on first boot
truncate --size 0 /etc/machine-id

yum -y update
#yum -y install cloud-init cloud-utils-growpart cern-config-users cern-private-cloud-addons NetworkManager-DUID-LLT mesa-libGL locmap
#rpm -e kernel-3.10.0-514.el7 linux-firmware
rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# fix rtc to use utc not local time (INC0974179)
# done via --utc timzezone switch
#timedatectl set-local-rtc 0
#cat >> /etc/adjtime << EOF
#0.0 0 0.0
#0
#UTC
#EOF

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: 1|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
    # and fix it again !
    sed -i -e 's/\[CLOUDINIT\]/cloud-init/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/syslogtag/programname/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/cloud-init.log/cloud-init-output.log/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

# enable locmap default modules
locmap --enable afs
locmap --enable kerberos
locmap --enable lpadmin
locmap --enable nscd
locmap --enable ntp
locmap --enable sendmail
locmap --enable ssh
locmap --enable sudo


# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,pre,post}.log
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/yum/*

# Ironic fix for RAID1 at boot time (Disabled for now)
/usr/bin/dracut -v --add 'mdraid' --add-drivers 'raid1 raid10 raid5 raid0 linear' --regenerate-all --force

# Ironic fix rd.auto
sed -i -e 's/crashkernel/rd.auto net.ifnames=0 crashkernel/g' /etc/default/grub

# We are installing bootloader with the bootloader command, but this only adds uefi compatible (as we are building in a UEFI VM), so we force both:https://unix.stackexchange.com/questions/273329/can-i-install-grub2-on-a-flash-drive-to-boot-both-bios-and-uefi
# Enable BIOS bootloader
grub2-mkconfig --output /etc/grub2-efi.cfg
grub2-install --target=i386-pc --directory=/usr/lib/grub/i386-pc/ /dev/vda
grub2-mkconfig --output=/boot/grub2/grub.cfg



# Fix grub.cfg to remove EFI entries and adapt them to BIOS

# blkid --match-tag UUID --output value /dev/vda15 on C8!!!!!
EFI_ID=`blkid -s UUID -o value /dev/vda15`
BOOT_ID=`blkid -s UUID -o value /dev/vda1`
sed -i 's/gpt15/gpt1/' /boot/grub2/grub.cfg
sed -i "s/${EFI_ID}/${BOOT_ID}/" /boot/grub2/grub.cfg
sed -i 's|${config_directory}/grubenv|(hd0,gpt15)/efi/centos/grubenv|' /boot/grub2/grub.cfg
sed -i '/^### BEGIN \/etc\/grub.d\/30_uefi/,/^### END \/etc\/grub.d\/30_uefi/{/^### BEGIN \/etc\/grub.d\/30_uefi/!{/^### END \/etc\/grub.d\/30_uefi/!d}}' /boot/grub2/grub.cfg
# Required for CentOS 7.x due to no blscfg: https://bugzilla.redhat.com/show_bug.cgi?id=1570991#c6
sed -i -e 's|linuxefi|linux|' -e 's|initrdefi|initrd|' /boot/grub2/grub.cfg


# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end

